{ pkgs, ... }:

{
  system.stateVersion = "22.05";
  networking.hostName = "valor";

  imports = [
    ../../modules/desktop.nix
  ];

  boot.initrd.availableKernelModules = ["xhci_pci" "nvme" "usb_storage" "sd_mod" "sdhci_pci"];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = ["kvm-intel" "i915" "acpi_call"];
  boot.extraModulePackages = with pkgs.linuxPackages; [ acpi_call ];

  impermanence.enable = true;

  hardware.bluetooth.enable = true;
  services.xserver.videoDrivers = ["modesetting"];
  hardware.graphics.extraPackages = with pkgs; [
    intel-vaapi-driver
    intel-media-driver
  ];

  environment.variables.VDPAU_DRIVER = "va_gl";

  swapDevices = [{ device = "/dev/disk/by-label/swap"; }];

  # Disable USB(??) Wakeup since it keeps waking us up immediately after suspend
  systemd.services.wakeup-suspend-fix = {
    enable = true;
    wantedBy = ["multi-user.target"];
    script = ''
      device=RP05

      if (cat /proc/acpi/wakeup | grep $device | grep -q enabled); then
        echo "$device is enabled, disabling it for wakeup"
        echo $device > /proc/acpi/wakeup
      else
        echo "$device is disabled, skip."
      fi

      exit 0
    '';
  };
}
