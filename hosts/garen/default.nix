{ pkgs, ... }:

with builtins;

{
  system.stateVersion = "22.11";
  networking.hostName = "garen";

  imports = [
    ../../modules/desktop.nix
    ../../modules/gaming.nix
  ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];
  boot.supportedFilesystems = [ "ntfs" ];

  impermanence.enable = true;
  impermanence.rootSize = "8G";
  fileSystems."/data" = {
    device = "/dev/sda1";
    fsType = "ntfs";
    options = [ "rw" "uid=1000" ];
  };

  swapDevices = [{ device = "/dev/disk/by-label/swap"; }];

  services.xserver.videoDrivers = [ "nvidia" ];
  boot.extraModprobeConfig = "options nvidia " + (concatStringsSep " "[
    # apparently nvidia still tries to support anchient CPUs by default
    "NVreg_UsePageAttributeTable=1"
    "NVReg_EnablePCIeGen3=1"
  ]);
  hardware.nvidia.package = pkgs.linuxPackages.nvidiaPackages.production;
  hardware.nvidia.modesetting.enable = true;

  # the documentation talks about this introducing bugs with suspend, but
  # it actually activates services that attempt to make it work?
  hardware.nvidia.powerManagement.enable = true;
  # hardware.nvidia.open = true;

  # Nvidia problems
  environment.sessionVariables = {
    # Necessary to correctly enable va-api (video codec hardware
    # acceleration). If this isn't set, the libvdpau backend will be
    # picked, and that one doesn't work with most things, including
    # Firefox.
    LIBVA_DRIVER_NAME = "nvidia";
    MOZ_DISABLE_RDD_SANDBOX = "1";
    EGL_PLATFORM = "wayland";
    # Required to run the correct GBM backend on wayland
    GBM_BACKEND = "nvidia-drm";
    __GLX_VENDOR_LIBRARY_NAME = "nvidia";
    __GL_VRR_ALLOWED = "1";
    # Hardware cursors do not work on nvidia
    WLR_NO_HARDWARE_CURSORS = "1";
    WLR_DRM_NO_ATOMIC = "1";

    # firefox fixes

  };

  programs.dconf.profiles.gdm.databases = [{
    settings = {
      "org/gnome/desktop/peripherals/mouse" = {
        accel-profile = "flat";
        speed = -0.85;
      };
    };
  }];

  systemd.tmpfiles.rules = [
    "L+ /run/gdm/.config/monitors.xml - - - - ${pkgs.writeText "monitors.xml" (readFile ./monitors.xml)}"
  ];
}
