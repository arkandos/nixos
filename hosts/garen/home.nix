{ lib, pkgs, ... }:

{
  # hyprland = {
  #   mouseSpeed = -0.85;
  #   monitors = [
  #     "DP-1,3840x2160,0x0,1.5"
  #     "DP2,2560x1440@144,2560x0,1"
  #   ];
  # };
  gnome.mouseSpeed = -0.85;
  
  home.file.".local/share/applications/league-of-legends.desktop".text = ''
    [Desktop Entry]
    Type=Application
    Name=League of Legends
    Icon=/home/arkan/Games/league-of-legends/drive_c/Riot Games/Riot Client/Resources/icon.ico
    Exec=env LUTRIS_SKIP_INIT=1 lutris lutris:rungameid/1
    Categories=Game
  '';

  home.file.".local/share/applications/windows-10.desktop".text = ''
    [Desktop Entry]
    Version=1.0
    Type=Application
    Terminal=false
    Exec=${lib.getExe pkgs.quickemu} --vm windows-10.conf --display spice --public-dir /home/arkan/Projects/creativITy
    Path=/home/arkan/Projects/creativITy/vm
    Name=windows-10
    Icon=qemu
  '';

  home.file.".config/monitors.xml".text = builtins.readFile ./monitors.xml;
}
