{ pkgs, ... }:

{
  home.stateVersion = "22.05";
  home.username = "arkan";
  home.homeDirectory = "/home/arkan";

  imports = [
    ../../home/core.nix
    ../../home/cli
    ../../home/gnome
    ../../home/vscode.nix
    ../../home/helix.nix
  ];

  git = {
    enable = true;
    name = "yoshi~";
    email = "jreusch4@gmail.com";

    includes = [
      {
        condition = "gitdir:~/Projects/creativITy/";
        contents = {
          user = {
            name = "Joshi Reusch";
            email = "joshua.reusch@creativity-gmbh.de";
          };
        };
      }
    ];
  };

  starship.enable = true;

  gnome = {
    enable = true;
    wallpaper = "/etc/nixos/wallpapers/297765.png"; # TODO: use file from derivation
    face = "/etc/nixos/users/arkan/face.jpg"; # TODO: use file from derivation
    latitude = 49.1;
    longitude = 12.5;
  };

  gtk.enable = true;
  gtk.gtk3.bookmarks = [
    "file:///home/arkan/Downloads"
    "file:///home/arkan/Projects"
    "file:///etc/nixos"
    "ssh://web-user@mein-creativity.shop mein-creativity.shop"
  ];

  vscode.enable = true;

  services.syncthing.enable = true;

  # TODO: At least one of the "keep-within", "keep-last", "keep-secondly", "keep-minutely", "keep-hourly", "keep-daily", "keep-weekly", "keep-monthly" or "keep-yearly" settings must be specified.
  # TODO: How do I deal with the fact that I have this twice? use 2 repositories?
  # TODO: check if I can download a backup.
  # services.borgmatic.enable = true;
  # programs.borgmatic = {
  #   enable = true;
  #   backups = {
  #     documents = {
  #       location = {
  #         sourceDirectories = [ "/home/arkan/Documents" ];
  #         repositories = [ "ssh://a57jtt5n@a57jtt5n.repo.borgbase.com/./repo" ];
  #       };
  #       storage.encryptionPasscommand = "${pkgs.coreutils-full}/bin/cat /etc/nixos/secrets/borgbase.pass";
  #       retention.keepHourly = 2;
  #       retention.keepDaily = 1;
  #       retention.keepWeekly = 8;
  #     };
  #   };
  # };

  programs.lazygit = {
    enable = true;
    settings = {
      os.editPreset = "helix";
      git.autoFetch = false; # prevent fetches on startup
      promptToReturnFromSubprocess = false;
    };
  };

  programs.zoxide.enable = true;

  home.packages = with pkgs; [
    jaq htmlq bottom
    # search tools
    ripgrep fastmod
    # network stuff
    tcpdump nmap

    hexyl # hex viewer
    scc # cloc but more fun

    chromium
    nodejs deno bun
    gleam erlang_27 rebar3
    git-absorb
    

    libreoffice gimp

    spotify discord
    
    netlify-cli glab
    
    # upwork # upwork is broken but I dont care right now.

    # Uni
    zotero
    (python3.withPackages (ps: with ps; [
      jupyter
      ipython

      numpy
      pandas
      matplotlib
      scikit-learn
      scipy
      seaborn
      statsmodels
      # openpyxl
      # sqlalchemy
      # librosa

      # tensorflow
      # keras
      # livelossplot
      # opencv4

      # pyppeteer
    ]))
  ];

  home.shellAliases = {
    top = "btm";
    jq = "jaq";
  };
}
