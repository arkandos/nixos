{ ... }:

let
  name = "arkan";
in
{
  users.users.${name} = {
    isNormalUser = true;
    name = name;
    initialHashedPassword = "$6$XMn2m4.Pg9$rAP//b7oEMRw1oHSXsLrYw1gStsSa7/DFRA5gPzu5S6bNBdfaBGl5b9hsLSqYr2ySbG5GoE33FKob0YZyRccf.";
    extraGroups = [
      "audio"
      "video"
      "input"
      "docker"
      "podman"
      "libvirtd"
      "networkmanager"
      "wheel"
    ];
  };

  environment.persistence."/nix/persist/users".users.${name} = {
    directories = (builtins.map (d: { directory = d; mode = "0700"; }) [
      ".config/doctl" # digitalocean cli
      ".config/glab-cli" # gitlab cli
      ".ssh" # ssh keys and config.
      ".cert" # used by borg
      ".gnupg" # GPG keys and metadata
      ".docker" # access key to the docker registry

    # Use symlinks for less security critical stuff, since they are faster
    # (source: the-mikedavis/dotfiles)
    # TODO: method does not work here.
    ]) ++ (builtins.map (d: { directory = d; }) [
      # Desktop directories      
      "Documents"
      "Projects"
      "Music"
      "Videos"
      "Pictures"
      "Public"

      # config, most of these apps abuse the config for state.
      ".mozilla"
      ".config/Slack"
      ".config/Teams"
      ".config/Insomnia"
      ".config/spotify"
      ".cache/spotify"
      ".cache/spotify-player"
      ".config/discord"
      ".config/syncthing" # syncthing legacy - TODO: move files on garen to new location
      ".local/state/syncthing"
      ".local/share/zoxide"

      # cache directories
      ".cache/fontconfig"
      ".cache/mozilla"
      ".local/state/nix"
      ".local/share/nix"
      ".cache/nix-index"
      ".cache/nix"
      
      # Zotero also seems to embed a browser somehow? QtWebKit?
      ".zotero"
      "Zotero"

      # Games are managed outside of NixOS, so persisting them is fine
      "Games"
      ".wine"
      ".config/lutris"
      ".local/share/lutris"
      ".local/share/Steam" # do NOT persist .steam; it just contains pid files and symlinks to here; if you do, Proton will crash FUSE spectacularly in one of the most annoying bugs to figure out possible!

      # ".local/share/waydroid" # waydroid user data
    ]);

    files = [
      ".bash_history"
      ".npmrc"
    ];
  };

  home-manager.users.arkan = ./home.nix;
}
