#!/bin/sh

set -xe

# 1. Partitioning

parted /dev/nvme0n1 -- mklabel gpt
parted /dev/nvme0n1 -- mkpart primary 512MB 64GB
parted /dev/nvme0n1 -- mkpart primary 64GB -32GB
parted /dev/nvme0n1 -- mkpart primary linux-swap -32GB 100%
parted /dev/nvme0n1 -- mkpart ESP fat32 1MB 512MB
parted /dev/nvme0n1 -- set 4 esp on

mkfs.ext4 -L nixos /dev/nvme0n1p1
mkfs.exit4 -L persist /dev/nvme0n1p2
mkswap -L swap /dev/nvme0n1p3
mkfs.fat -F 32 -n boot /dev/nvme0n1p4

# 2. Mounting

mount -t tmpfs tmpfs /mnt -o size=2G

mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot

mkdir -p /mnt/nix
mount /dev/disk/by-label/nixos /mnt/nix

mkdir -p /mnt/nix/persist/users
mount /dev/disk/by-label/persist /mnt/nix/persist/users

# Impermanence setup
mkdir -p /mnt/etc/nixos
mkdir -p /mnt/nix/persist/system/etc/nixos
mount --bind /mnt/nix/persist/system/etc/nixos /mnt/etc/nixos

mkdir -p /mnt/root
mkdir -p /mnt/nix/persist/root
mount --bind /mnt/nix/persist/root /mnt/root

# Install System

cd /mnt/etc/nixos
git clone git@gitlab.com:arkandos/nixos.git .
nixos-install --no-root-password --flake '.#garen'
