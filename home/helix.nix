{ pkgs, ... }:

{
	# Keybindings TODO:
	# - insert cursor at next selection (so if I select word, I want to extend the selection with the next occurrence of word)

	# maybe change w/b to extend selection by default, and use some other keybind to skip words (I think there already is one)

	# Advanced keymaps I forget:
	# - Toggle Comment: C-c
	# - Insert cursor below: S-c
	# - Indent/Unindent: >/<
	# - LS movement: m
	
  programs.helix = {
		enable = true;
		package =
			let
				withCustomLanguageServers = pkgs.helix.overrideAttrs (self: {
					makeWrapperArgs = with pkgs; self.makeWrapperArgs or [] ++ [
						"--suffix"
						"PATH"
						":"
						(lib.makeBinPath [
							nil # nixos
							taplo # toml
							clang-tools # c, c++
							gopls # go
							marksman # markdown
							nodePackages.bash-language-server # shell
							# nodePackages.vscode-css-languageserver-bin #css
							nodePackages.vscode-langservers-extracted # html
							nodePackages.typescript-language-server # ts, js
							rust-analyzer # rust
						])
					];
				});
		in withCustomLanguageServers;

		settings = {
			theme = "nord";
			editor = {
				auto-save = true;
				idle-timeout = 0;

				true-color = true;
				color-modes = true;
				rainbow-brackets = true;
				
				cursorline = true;
				cursor-shape = {
					insert = "bar";
					normal = "block";
					select = "underline";
				};
				
				rulers = [80 120];
				text-width = 120;
				indent-guides = {
					render = true;
					skip-levels = 1;
					rainbow-option = "dim";
					character = "┊";
				};
				soft-wrap.enable = true;

				end-of-line-diagnostics = "hint";
				inline-diagnostics = {
					cursor-line = "warning";
				};
				lsp.display-inlay-hints = true;
			};

			keys.normal = {
				"C-f" = [":format"];
				# simpler way to move lines/selection up/down
				"C-j" = ["extend_to_line_bounds" "delete_selection" "paste_after"];
				"C-down" = ["extend_to_line_bounds" "delete_selection" "paste_after"];
				"C-k" = ["extend_to_line_bounds" "delete_selection" "move_line_up" "paste_before"];
				"C-up" = ["extend_to_line_bounds" "delete_selection" "move_line_up" "paste_before"];
				# "deselect" a line, select upwards
				X = ["extend_line_up" "extend_to_line_bounds"];
			};

			keys.select = {
				X = ["extend_line_up" "extend_to_line_bounds"];
			};
		};
	};

	home.sessionVariables.EDITOR = "hx";
	home.sessionVariables.VISUAL = "hx";
}
