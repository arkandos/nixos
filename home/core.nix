{ osConfig, ... }:
{
  imports = [
    ../hosts/${osConfig.networking.hostName}/home.nix
  ];

  manual.json.enable = false;
  manual.html.enable = false;
  manual.manpages.enable = false;

  programs.home-manager.enable = true;

  systemd.user.startServices = true;
}
