{ ... }:

{
  imports = [
    ./git.nix
    ./starship.nix
  ];
}
