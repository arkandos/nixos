{ config, lib, ... }:

let
  cfg = config.starship;
in
{
  options.starship = {
    enable = lib.mkEnableOption "Enable starship";
  };

  config = lib.mkIf cfg.enable {
  # needed for home-manager to write a bashrc file, which we need for starship.enableBashIntegration
    programs.bash.enable = true;

    programs.starship = {
      enable = true;
      enableBashIntegration = true;
      settings = {
        character = {
          success_symbol = "[›](bold green)";
          error_symbol = "[›](bold red)";
        };

        git_status = {
          deleted = "✗";
          modified = "✶";
          staged = "✓";
          stashed = "≡";
        };

        nix_shell = {
          symbol = " ";
          heuristic = true;
        };
      };
    };
  };
}
