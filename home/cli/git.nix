{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.git;
in
{
  options.git = {
    enable = mkEnableOption "Enable git integration";

    name = mkOption {
      type = types.str;
    };

    email = mkOption {
      type = types.str;
    };

    includes = mkOption {
      type = types.listOf (types.submodule {
        options = {
          condition = mkOption {
            type = types.str;
          };
          contents = mkOption {
            type = types.attrsOf types.anything;
          };
        };
      });
      default = [];
    };
  };

  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      gitAndTools.git git-lfs delta
    ];
  
    programs.git = {
      enable = true;
      userName = cfg.name;
      userEmail = cfg.email;
      includes = cfg.includes;

      delta.enable = true;

      extraConfig = {
        hub.protocol = "ssh";
        pull.rebase = false;
        init.defaultBranch = "main";
        push.default = "simple";
        commit.verbose = "true";
        
        diff.colorMoved = "default";
        push.autoSetupRemote = true;
        push.followTags = true;
        safe.directory = ["/etc/nixos"];
      };

      aliases = {
        a = "add";
        b = "branch";
        c = "commit";
        ca = "commit --amend";
        cm = "commit -m";
        co = "checkout";
        d = "diff";
        ds = "diff --staged";
        p = "push";
        pf = "push --force-with-lease";
        pl = "pull";
        l = "log";
        r = "rebase";
        s = "status --short";
        ss = "status";
        forgor = "commit --amend --no-edit";
        graph = "log --all --decorate --graph --oneline";
        oops = "checkout --";
        dl = "clone --depth=1 --single-branch";
        unstage = "reset HEAD --";
      };

      ignores = ["*~" "*.swp" ".direnv" "result"];
    };

    # scrolling in git diff
    home.sessionVariables.DELTA_PAGER = "less -R";
  };
}
