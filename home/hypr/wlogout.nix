{ config, pkgs, lib, ... }:

let
  cfg = config.hyprland;
  script = lib.getExe pkgs.desktopctl;
in
lib.mkIf cfg.enable {
  programs.wlogout = {
    enable = true;
    layout = [
      {
        label = "lock";
        action = "${script} lock";
        text = "Lock";
        keybind = "l";
      }
      {
        label = "logout";
        action = "${script} logout";
        text = "Logout";
        keybind = "e";
      }
      {
        label = "shutdown";
        action = "${script} shutdown";
        text = "Shutdown";
        keybind = "s";
      }
      {
        label = "reboot";
        action = "${script} reboot";
        text = "Reboot";
        keybind = "r";
      }
    ];

    style = ''
      * {
          background-image: none;
          font-size: 22px;
      }

      @define-color bar-bg rgba(0, 0, 0, 0);

      @define-color main-bg #faf4ed;
      @define-color main-fg #907aa9;

      @define-color wb-hvr-bg #b4637a;
      @define-color wb-hvr-fg #fffaf3;

      @define-color wb-act-bg #56949f;
      @define-color wb-act-fg #fffaf3;

      window {
          background-color: transparent; /* rgba(255, 255, 255, 0.2); */
      }

      button {
          color: black;
          background-color: @main-bg;
          outline-style: none;
          border: none;
          border-width: 0px;
          background-repeat: no-repeat;
          background-position: center;
          background-size: 10%;
          border-radius: 0px;
          box-shadow: none;
          text-shadow: none;
          animation: gradient_f 20s ease-in infinite;
      }

      button:focus {
          background-color: @wb-act-bg;
          background-size: 20%;
      }

      button:hover {
          background-color: @wb-hvr-bg;
          background-size: 25%;
          border-radius: 60px;
          animation: gradient_f 20s ease-in infinite;
          transition: all 0.3s cubic-bezier(.55,0.0,.28,1.682);
      }

      button:hover#lock {
          border-radius: 60px 60px 0px 60px;
          margin : 240px 0px 0px 640px;
      }

      button:hover#logout {
          border-radius: 60px 0px 60px 60px;
          margin : 0px 0px 240px 640px;
      }

      button:hover#shutdown {
          border-radius: 60px 60px 60px 0px;
          margin : 240px 640px 0px 0px;
      }

      button:hover#reboot {
          border-radius: 0px 60px 60px 60px;
          margin : 0px 640px 240px 0px;
      }

      #lock {
          background-image: image(url("/etc/nixos/icons/lock_black.png"));
          border-radius: 96px 0px 0px 0px;
          margin : 296px 0px 0px 696px;
      }

      #logout {
          background-image: image(url("/etc/nixos/icons/logout_black.png"));
          border-radius: 0px 0px 0px 96px;
          margin : 0px 0px 296px 696px;
      }

      #shutdown {
          background-image: image(url("/etc/nixos/icons/shutdown_black.png"));
          border-radius: 0px 96px 0px 0px;
          margin : 296px 696px 0px 0px;
      }

      #reboot {
          background-image: image(url("/etc/nixos/icons/reboot_black.png"));
          border-radius: 0px 0px 96px 0px;
          margin : 0px 696px 296px 0px;
      }

    '';
  };
}
