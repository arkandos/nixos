{ config, lib, pkgs, ... }:

let
  cfg = config.hyprland;
  script = lib.getExe pkgs.desktopctl;
  swaylock = lib.getExe pkgs.swaylock;
in
lib.mkIf cfg.enable {
  systemd.user.services.hypridle = {
    Unit = {
      Description = "Hyprland's idle management daemon";
      PartOf = ["graphical-session.target"];
    };

    Service = {
      ExecStart = "${lib.getExe pkgs.hypridle}";
      Restart = "on-failure";
    };

    Install.WantedBy = ["graphical-session.target"];
  };

  xdg.configFile."hypr/hypridle.conf".text = ''
    general {
      lock_cmd = ${swaylock} -f
      before_sleep_cmd = ${script} lock
    }

    listener {
      timeout = 300
      on-timeout = loginctl lock-session
    }

    listener {
      timeout = 600
      on-timeout = ${script} screen-off
      on-resume = ${script} screen-on
    }

    listener {
      timeout = 3600
      on-timeout = ${script} suspend
    } 
  '';
}
