{ config, lib, pkgs, ... }:

let
  cfg = config.hyprland;
in
lib.mkIf cfg.enable {
  systemd.user.services.swaybg = {
    Unit = {
      Description = "Set desktop wallpaper";
      PartOf = ["graphical-session.target"];
    };

    Service = {
      ExecStart = "${lib.getExe pkgs.swaybg} -i ${cfg.wallpaper} -m fill";
      Restart = "on-failure";
    };

    Install.WantedBy = ["graphical-session.target"];
  };}
