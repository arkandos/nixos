{ config, lib, pkgs, ... }:

let
  cfg = config.hyprland;

  
  inherit (config.lib.formats.rasi) mkLiteral;


  px = v: mkLiteral "${toString v}px";
  main-bg = mkLiteral "#faf4ede6";
  main-fg = mkLiteral "#b4637aff";
  main-br = mkLiteral "#9893a5ff";
  select-bg = mkLiteral "#d7828eff";
  select-fg = mkLiteral "#fffaf3ff";
  transparent = mkLiteral "transparent";
in
lib.mkIf cfg.enable {
  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    terminal = "kitty";
    font = "FireCode Nerd Font 14";
    
    extraConfig = {
      modi = "drun,filebrowser,window,run";
      show-icons = true;
      icon-theme = "Papirus-Dark";
      display-drun = "";
      display-run = "";
      display-filebrowser = "";
      display-window = "";
      drun-display-format = "{name}";
      window-format = "{w}{t}";
    };

    theme = {
      "*" = {
        separatorcolor = transparent;
        border-color = transparent;
      };

      window = {
        height = px 590;
        width = px 1140;
        transparency = "real";
        fullscreen = false;
        enabled = true;
        cursor = "default";
        spacing = px 0;
        padding = px 0;
        border = px 4;
        border-radius = px 36;
        border-color = main-br;
        background-color = transparent;
      };
    
      mainbox = {
        enabled = true;
        spacing = px 0;
        orientation = mkLiteral "horizontal";
        children = ["inputbar" "listbox"];
        background-color = transparent;
        background-image = mkLiteral "url(\"${cfg.wallpaper}.blur\", height)";
      };


      inputbar = {
        enabled = true;
        width = px 700;
        children = ["mode-switcher" "entry"];
        background-color = transparent;
        background-image = mkLiteral "url(\"${cfg.wallpaper}\", height)";
      };
    
      entry = {
        enabled = false;
      };

      mode-switcher = {
        orientation = mkLiteral "vertical";
        enabled = true;
        width = px 68;
        padding = mkLiteral "160px 10px 160px 10px";
        spacing = px 25;
        background-color = transparent;
        background-image = mkLiteral "url(\"${cfg.wallpaper}.blur\", height)";
      };
    
      button = {
        cursor = mkLiteral "pointer";
        border-radius = px 50;
        background-color = main-bg;
        text-color = main-fg;
      };
    
      "button selected" = {
        background-color = main-fg;
        text-color = main-bg;
      };

      listbox = {
        spacing = px 10;
        padding = px 30;
        children = ["listview"];
        background-color = main-bg;
      };
    
      listview = {
        enabled = true;
        columns = 1;
        cycle = true;
        dynamic = true;
        scrollbar = false;
        layout = mkLiteral "vertical";
        reverse = false;
        fixed-height = true;
        fixed-columns = true;
        cursor = "default";
        background-color = transparent;
        text-color = main-fg;
      };

      element = {
        enabled = true;
        spacing = px 30;
        padding = px 8;
        border-radius = px 24;
        cursor = mkLiteral "pointer";
        background-color = transparent;
        text-color = main-fg;
      };

      "element selected.normal" = {
        background-color = select-bg;
        text-color = select-fg;
      };
    
      element-icon = {
        size = px 48;
        cursor = mkLiteral "inherit";
        background-color = transparent;
        text-color = mkLiteral "inherit";
      };
    
      element-text = {
        vertical-align = mkLiteral "0.5";
        horizontal-align = 0;
        cursor = mkLiteral "inherit";
        background-color = transparent;
        text-color = mkLiteral "inherit";
      };
    };
  };
}
