{ config, lib, ... }:

let
  cfg = config.hyprland;
in
lib.mkIf cfg.enable {  
  services.wlsunset = {
    enable = true;
    latitude = "${builtins.toString cfg.latitude}";
    longitude = "${builtins.toString cfg.longitude}";
    systemdTarget = "hyprland-session.target";
  };
}
