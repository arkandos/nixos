{ config, lib, ... }:

let
  cfg = config.hyprland;
in
lib.mkIf cfg.enable { 
  services.mako = {
    enable = true;
    layer = "overlay";
    defaultTimeout = 10 * 1000; # ms
    maxVisible = 5;
    # output = cfg.primaryMonitor;
    anchor = "top-right";
    format = "<small>󰟪  %a\\n</small><b>󰋑  %s</b>\\n%b"; # double escape because nixexpr bug

    backgroundColor = "#faf4ede6";
    textColor = "#b4637aff";
    borderColor = "#9893a5ff";
    borderRadius = 12;
    borderSize = 4;
    padding = "8";

    font = "Inter 11";

    extraConfig = ''
      outer-margin=20,10
      
      [urgency=critical]
      background-color=#f5e0dcff
      text-color=#1e1e2eff
      border-color=#f38ba8ff
    '';
    };
  }
