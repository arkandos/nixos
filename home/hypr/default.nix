{ config, pkgs, lib, ... }:

with lib;

# TODO:
#  - extract colors and set them in options

let
  cfg = config.hyprland;
in
{
  options.hyprland = {
    enable = mkEnableOption "Configure Hyprland and a bunch of other tools to have a fully functioning desktop environment";

    wallpaper = mkOption {
      type = types.str;
    };

    mouseSpeed = mkOption {
      type = types.float;
      default = 1.0;
    };

    numlockActive = mkOption {
      type = types.bool;
      default = true;
    };

    monitors = mkOption {
      type = types.listOf types.str;
      default = [];
    };

    longitude = mkOption {
      type = types.float;
    };

    latitude = mkOption {
      type = types.float;
    };
  };

  imports = [
    ./gtk.nix # compatibility
    ./hyprland.nix # window manager
    ./mime.nix # default apps
    ./swaybg.nix # wallpaper
    ./rofi.nix # app launcher
    ./mako.nix # notifications
    ./waybar.nix # top bar
    ./swaylock.nix # screen lock
    ./hypridle.nix # automatic lock, screen off
    ./wlogout.nix # logout popup
    ./wlsunset.nix # screen dim
    ./kitty.nix # terminal
  ];

  config = mkIf cfg.enable {
  
    home.pointerCursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Classic";
      size = 24;
      gtk.enable = true;
      x11.enable = true;
    };

    home.packages = with pkgs; [
      desktopctl
      xdg-utils # xdg-open, etc
      wl-clipboard # clipboard for wayland
      pavucontrol # sound control
      nautilus # file manager
      imv # image viewer
      evince # document viewer
      file-roller # archives
      totem # video player
      firefox # browser
    ];
  };
}
