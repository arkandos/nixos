{ config, lib, pkgs, ... }:

let
  cfg = config.hyprland;
in
lib.mkIf cfg.enable {  
  programs.waybar = {
    enable = true;

    systemd.enable = true;
    systemd.target = "hyprland-session.target";

    settings = [{
      layer = "top";
      position = "top";
      margin-top = 8;
      margin-left = 8;
      margin-right = 8;
      margin-bottom = 0;
      spacing = 4;
      modules-left = [
        # "privacy"
        "idle_inhibitor"
        "hyprland/workspaces"
        # "wlr/taskbar" # icons are broken in systemd
        # "hyprland/window"
      ];
      modules-center = [
        "mpris"
      ];
      modules-right = [
        "tray"
        "wireplumber"
        # "custom/docker"
        "cpu"
        "memory"
        "disk"
        "battery"
        "hyprland/language"
        "clock"
      ];
    
      idle_inhibitor = {
        format = "{icon}";
        format-icons.activated = " ";
        format-icons.deactivated = " ";
      };
    
      "hyprland/workspaces" = {
        format = "{icon}";
        format-icons.visible = "";
        format-icons.default = "●";
      };
    
      "wlr/taskbar" = {
        on-click = "activate";
        on-middle-click = "close";
        sort-by-app-id = true;
        all-outputs = false;
      };
    
      "hyprland/window" = {
        separate-outputs = true;
      };
    
      mpris = {
        interval = 1;
        format = "";
        format-playing = "{title} - {artist}";
      };
    
      wireplumber = {
        format = "{icon}";
        tooltip-format = "{node_name} - {volume}%";
        format-muted = "";
        reverse-scrolling = true;
        format-icons = ["" "" ""];
        on-click = "pavucontrol";
      };

      "custom/docker" = {
        format = "🐋 {}";
        interval = 10;
        tooltip = true;
        exec = "sh -c 'docker ps --quiet | wc -l; docker ps --format \"{{ .ID }} - {{ .Names }} ({{ .Image }})\"; echo'";
      };
    
      cpu = {
        interval = 10;
        format = "  {usage}%";
        format-icons = [
          "<span color='#69ff94'>▁</span>"
          "<span color='#2aa9ff'>▂</span>"
          "<span color='#f8f8f2'>▃</span>"
          "<span color='#f8f8f2'>▄</span>"
          "<span color='#ffffa5'>▅</span>"
          "<span color='#ffffa5'>▆</span>"
          "<span color='#ff9977'>▇</span>"
          "<span color='#dd532e'>█</span>"
        ];
      };
    
      memory = {
        interval = 10;
        format = "  {used:0.1f}G";
      };
    
      disk = {
        interval = 30;
        format = "🖴  {used}";
      };
    
      battery = {
        interval = 30;
        states.warning = 30;
        states.critical = 15;
        format = "{icon}  {power:.1f}W";
        format-charging = "⚡ {time}";
        format-time = "{H}:{m}";
        format-icons = [ " " " " " " " " " " ];
      };
    
      "hyprland/language" = {
        format-en = "🇺🇸";
        format-de = "🇩🇪";
      };
    
      clock = {
        format = "  <b>{0:%H}</b>:{0:%M}";
        format-alt = "  {:%A, %B %d, %Y (%R)}";
        tooltip-format = "<tt><small>{calendar}</small></tt>";
      };
      clock.calendar = {
        mode = "year";
        mode-mon-col = 3;
        on-scroll = 1;
        on-click-right = "mode";
      
        format.months = "<span color='#ffead3'><b>{}</b></span>";
        format.days = "<span color='#ecc6d9'><b>{}</b></span>";
        format.weeks = "<span color='#99ffdd'><b>W{}</b></span>";
        format.weekdays = "<span color='#ffcc66'><b>{}</b></span>";
        format.today = "<span color='#ff6699'><b><u>{}</u></b></span>";
      };
      clock.actions = {
        on-click-right = "mode";
        on-scroll-up = "shift_up";
        on-scroll-down = "shift_down";
      };
    }];

    style = ''
      * {
        /* font-family: Inter, "Noto Color Emoji"; */
        border: none;
        border-radius: 0;
        font-size: 12px;
      }

      @define-color rosewater #f4dbd6;
      @define-color flamingo #f0c6c6;
      @define-color pink #f5bde6;
      @define-color mauve #c6a0f6;
      @define-color red #ed8796;
      @define-color maroon #ee99a0;
      @define-color peach #f5a97f;
      @define-color yellow #eed49f;
      @define-color green #a6da95;
      @define-color teal #8bd5ca;
      @define-color sky @91d7e3;
      @define-color sapphire #7dc4e4;
      @define-color blue #8aadf4;
      @define-color lavender #b7bdf8;
      @define-color text #cad3f5;
      @define-color subtext1 #b8c0e0;
      @define-color subtext0 #a5adcb;
      @define-color overlay2 #939ab7;
      @define-color overlay1 #8087a2;
      @define-color overlay0 #6e738d;
      @define-color surface2 #5b6078;
      @define-color surface1 #494d64;
      @define-color surface0 #363a4f;
      @define-color base #24273a;
      @define-color mantle #1e2030;
      @define-color crust #181926;

      window#waybar {
        background: @base;
        border-radius: 9999px;
        color: @blue;
        font-weight: 600;
      }

      .modules-left, .modules-right, #mpris {
        margin: 6px;
        border: 4px solid @flamingo;
        background: rgb(250, 244, 237);
        border-radius: 9999px;
      }

      .modules-right {
        color: @blue;
      }

      .modules-left {
        color: @pink;
      }

      .modules-center {
        color: @mauve;
      }

      .modules-left>widget>*,
      .modules-right>widget>*,
      .modules-center>widget>* {
        padding-left: 16px;
        padding-right: 16px;

      }

      .modules-right>widget>*{
        padding-left: 8px;
        padding-right: 8px;
      }

      #workspaces {
        padding: 0;
      }

      #idle_inhibitor {
        color: @blue;
        padding-left: 12px;
        padding-right: 8px;
      }

      #workspaces button {
        background: transparent;
        border-radius: 9999px;
        padding: 4px;
        min-height: 0;
      }

      #workspaces button:hover {
        transition: none;
        box-shadow: inherit;
        text-shadow: inherit;
        color: @sapphire;
      }

      #workspaces button.visible {
        background: @flamingo;
        color: @sky;
      }

      #wireplumber {
        font-size: 16px;
      }

      #disk {
        padding-top: 2px;
      }

      #clock {
        padding-top: 2px;
      }



      @keyframes blink {
        to {
          background-color: #ffffff;
          color: #000000;
        }
      }

      #battery.critical:not(.charging) {
        background-color: #f53c3c;
        color: #ffffff;
        animation-name: blink;
        animation-duration: 0.5s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        animation-direction: alternate;
      }

      label:focus {
        background-color: #000000;
      }

      #tray>.needs-attention {
        -gtk-icon-effect: highlight;
        background-color: #eb4d4b;
      }
    '';
  };
    
  # everything in path for waybar
  systemd.user.services.waybar.Service.Environment = "PATH=/run/current-system/sw/bin:${config.home.profileDirectory}/bin:$PATH";

  services.playerctld.enable = true;


  systemd.user.services.nm-applet = {
    Unit = {
      Description = "NetworkManager Applet";
      PartOf = ["hyprland-session.target"];
    };

    Install.WantedBy = ["hyprland-session.target"];

    Service = {
      ExecStart = "${lib.getExe pkgs.networkmanagerapplet}";
      Restart = "on-failure";
    };
  };

  # proxy old XEmbed widgets (i.e. wine Riot Games) to use SNI and be compatible with waybar
  services.xembed-sni-proxy.enable = true;
}
