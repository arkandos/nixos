{ config, lib, pkgs, ... }:

let
  cfg = config.hyprland;
in
lib.mkIf cfg.enable {
  gtk = {
    enable = true;

    font = {
      name = "Inter";
      package = pkgs.inter;
      size = 11;
    };

    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };

    theme = {
      name = "Catppuccin-Macchiato-Standard-Flamingo-Dark";
      package = pkgs.catppuccin-gtk.override {
        accents = [ "flamingo" ];
        size = "standard";
        variant = "macchiato";
      };
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "gtk"; # gnome
    style.name = "gtk2"; # adwaita
  };

  # make GTK4 apps (nautilus) behave
  home.sessionVariables.GTK_THEME = "Catppuccin-Macchiato-Standard-Flamingo-Dark";
}
