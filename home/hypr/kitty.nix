{ config, lib, pkgs, ... }:

let
  cfg = config.hyprland;
in
lib.mkIf cfg.enable {

  # TODO:
  # - read the rest of the docs to find cool other stuff to try
  # - why do we specify a background-opacity here and in hypr?
  programs.kitty = {
    enable = true;
    shellIntegration.enableBashIntegration = true;
    font = {
      size = 14;
      name = "FiraCode Nerd Font";
    };

    settings = {
      scrollback_lines = 10000;
      placement_strategy = "center";

      allow_remote_control = "yes";
      enable_audio_bell = "no";
      visual_bell_duration = "0.1";

      copy_on_select = "clipboard";

      selection_foreground = "none";
      selection_background = "none";

      # colors
      background_opacity = "0.9";
    };

    theme = "Catppuccin-Macchiato";

    keybindings = {
      "kitty_mod+t" = "new_tab_with_cwd";
    };
  };

  # change color scheme on ssh
  xdg.configFile."kitty/ssh.conf".text = ''
    hostname *
    color_scheme ${pkgs.kitty-themes}/share/kitty-themes/themes/Sakura_Night.conf
  '';

  programs.bash.shellAliases.ssh = "kitten ssh";

  # set kitty to be the default terminal
  home.sessionVariables.TERM = "kitty";
  home.sessionVariables.TERMINAL = "kitty";

}
