{ config, pkgs, lib, ... }:

let
  script = lib.getExe pkgs.desktopctl;
  cfg = config.hyprland;
in
lib.mkIf cfg.enable {
  
  wayland.windowManager.hyprland = {
    enable = true;

    systemd.enable = true;
    xwayland.enable = true;

    settings = {
      "$globalMod" = "SUPER";
      "$windowMod" = "ALT";

      # assign apps
      "$term" = "kitty";
      "$editor" = "code";
      "$file" = "nautilus";
      "$browser" = "firefox";
      "$script" = "${script}";
      "$wlogout" = "${lib.getExe pkgs.wlogout}";
      "$rofi" = "${pkgs.rofi-wayland}/bin/rofi";

      env = [
        "XDG_CURRENT_DESKTOP,Hyprland"
        "XDG_SESSION_DESKTOP,Hyprland"
        "QT_WAYLAND_DISABLE_WINDOWDECORATION,1"
      ];

      xwayland.force_zero_scaling = true;

      misc = {
        disable_hyprland_logo = true;
        disable_splash_rendering = true;
        force_default_wallpaper = 0;

        mouse_move_enables_dpms = true;
        key_press_enables_dpms = true;

        disable_autoreload = true;
      };

      # MONITORS, INPUT
      monitor = cfg.monitors;
          
      input = {
        kb_layout = "us,de";
        kb_options = "grp:win_space_toggle,eurosign:e,lv3:ralt_switch";

        numlock_by_default = cfg.numlockActive;

        # 0 -> disabled
        # 1 -> always follow mouse
        # 2 -> will focus the mouse, but no the keyboard (only on click)
        # 3 -> mouse can focus, but keyboard will never follow
        follow_mouse = 1;
        touchpad.natural_scroll = true;

        sensitivity = cfg.mouseSpeed;
        accel_profile = "flat";
      };

      gestures = {
        workspace_swipe = true;
        workspace_swipe_fingers = 3;
        workspace_swipe_forever = true;
      };

      # LAYOUT
      dwindle = {
        pseudotile = true;
        preserve_split = true;
      };

      master.new_status = "inherit";
      general.layout = "master";
      # general.resize_on_border = true;

      # ANIMATION
      animations = {
        enabled = true;
        bezier = [
          "wind, 0.05, 0.9, 0.1, 1.05"
          "winIn, 0.1, 1.1, 0.1, 1.1"
          "winOut, 0.3, -0.3, 0, 1"
          "liner, 1, 1, 1, 1"
        ];
        animation = [
          "windows, 1, 6, wind, slide"
          "windowsIn, 1, 6, winIn, slide"
          "windowsOut, 1, 5, winOut, slide"
          "windowsMove, 1, 5, wind, slide"
          "border, 1, 1, liner"
          # "borderangle, 1, 30, liner, loop"
          "borderangle, 0"
          "fade, 1, 10, default"
          "workspaces, 1, 5, wind"
        ];
      };

      # THEME
      general = {
        gaps_in = 8;

        gaps_out = 14;
        border_size = 4;

        # TODO: extract / unify color variables
        "col.active_border" = "rgba(b4637aff) rgba(d7827eff) 45deg";
        "col.inactive_border" = "rgba(286983cc) rgba(56949fcc) 45deg";
      };

      group = {
        # TODO: better group tab colors
        "col.border_active" = "rgba(b4637aff) rgba(d7827eff) 45deg";
        "col.border_inactive" = "rgba(286983cc) rgba(56949fcc) 45deg";
        "col.border_locked_active" = "rgba(b4637aff) rgba(d7827eff) 45deg";
        "col.border_locked_inactive" = "rgba(286983cc) rgba(56949fcc) 45deg";
        # https://github.com/hyprwm/Hyprland/pull/3197
        # when merged make a groupbar style
      };

      decoration = {
        rounding = 12;
        drop_shadow = true;
        shadow_ignore_window = true;
        shadow_offset = "9 10";
        shadow_range = 5;
        shadow_render_power = 4;
        "col.shadow" = "0xff26233a";

        dim_special = 0.3;

        blur = {
          enabled = true;
          size = 6;
          passes = 3;
          new_optimizations = true;
          ignore_opacity = true;
          xray = false;
          special = true;
        };
      };

      # WINDOW RULES
      windowrulev2 = [
        # prevent swayidle while there is a fullscreen app active
        "idleinhibit fullscreen,class:^(.*)$"

        # prevent idle while there is a xwayland app in focus - 
        # this is a heuristic to detect games.
        "idleinhibit focus,xwayland:1"
        "opacity 1.0 override 0.7 override,xwayland:1"
        "float,class:gamescope"
        "opacity 1.0 override 0.7 override,class:gamescope"
        # since we use that, we probably also can do this heuristic
        
        # Opacity rules
        "opacity 0.9 0.9,class:^"
        "opacity 0.80 0.80,class:^(kitty)$"
        "opacity 0.80 0.80,class:^(discord)$ #Discord-Electron"
        "opacity 0.70 0.70,title:^(Spotify)$"
        "opacity 0.7 0.7,floating:1"

        # Apps to float by default
        "float,class:^(imv)$"
        "opacity 1.0 override 1.0 override,class:^(imv)$"
        
        "float,class:^(org.gnome.Nautilus)$"
        "float,class:^(Steam)$"
        "float,class:^(steam)$"
        "float,class:^(steamwebhelper)$"
        "float,class:^(nm-applet)$"
        "float,class:^(nm-connection-editor)$"
        "float,class:^(pavucontrol)$"

        "opacity 1.0 override 1.0 override,class:^(league of legends.exe)$"
        "monitor DP-2,class:^(league of legends.exe)$"
        "center,class:^(league of legends.exe)$"
        # windowrulev2 = float,class:^(qt5ct)$
        # windowrulev2 = float,class:^(nwg-look)$
        # windowrulev2 = float,class:^(org.kde.ark)$
        # windowrulev2 = float,class:^(Signal)$ #Signal-Gtk
        # windowrulev2 = float,class:^(com.github.rafostar.Clapper)$ #Clapper-Gtk
        # windowrulev2 = float,class:^(app.drey.Warp)$ #Warp-Gtk
        # windowrulev2 = float,class:^(net.davidotek.pupgui2)$ #ProtonUp-Qt
        # windowrulev2 = float,class:^(yad)$ #Protontricks-Gtk
        # windowrulev2 = float,class:^(io.gitlab.theevilskeleton.Upscaler)$ #Upscaler-Gtk
        # windowrulev2 = float,class:^(blueman-manager)$
        # windowrulev2 = float,class:^(org.kde.polkit-kde-authentication-agent-1)$
      ];

      layerrule = [
        # wlogout blur
        "blur, gtk-layer-shell"
      ];

      # KEYBINDS
      bind = [
        # Session actions
        "$globalMod, L, exec, $script lock" # lock screen
        "$globalMod, backspace, exec, $wlogout -b 2 -c 0 -r 0 -m 0 --protocol layer-shell" # logout menu

        # Keyboard layout
        "$globalMod, space, exec, $script toggle-xkb-layout" # change keyboard layout

        # Screenshot/Screencapture
        ", print, exec, $script screenshot-area" # drag to snip an area / click on a window to print it
        "$globalMod, print, exec, $script screenshot-monitor" # print focused monitor
        "$globalMod ALT, print, exec, $script screenshot-screen" # print the entire screen

        # Application shortcuts
        "$globalMod, R, exec, pkill -x rofi || $rofi -show drun" # Rofi menu
        "$globalMod, return, exec, $term"  # open terminal
        "$globalMod, E, exec, $file" # open file manager
        "$globalMod, C, exec, $editor" # open vscode
        "$globalMod, W, exec, $browser" # open browser
        "CTRL ALT, delete, exec, gnome-system-monitor" # system monitor
        # bind = $globalMod, V, exec, pkill -x rofi || ~/.config/hypr/scripts/cliphist.sh c  # open Pasteboard in screen center
        # bind = $globalMod, period, exec, emote # open emoji selector // install emote

        # Mute / unmute
        ", XF86AudioMute, exec, $script toggle-mute" # toggle audio mute
        "CTRL, F10, exec, $script toggle-mic-mute" # toggle mic mute (desktop)
        ", XF86AudioMicMute, exec, $script toggle-mic-mute" # toggle microphone mute
        
        # Music player control
        ", XF86AudioPlay, exec, $script play-pause"
        ", XF86AudioPause, exec, $script play-pause"
        ", XF86AudioNext, exec, $script play-next"
        ", XF86AudioPrev, exec, $script play-prev"


        # Work with workspaces

        # Switch workspaces with mainMod + [0-9]
        "$globalMod, 1, workspace, 1"
        "$globalMod, 2, workspace, 2"
        "$globalMod, 3, workspace, 3"
        "$globalMod, 4, workspace, 4"
        "$globalMod, 5, workspace, 5"
        "$globalMod, 6, workspace, 6"
        "$globalMod, 7, workspace, 7"
        "$globalMod, 8, workspace, 8"
        "$globalMod, 9, workspace, 9"
        "$globalMod, 0, workspace, 10"

        # Switch workspaces relative to the active workspace with mainMod + [←→]
        "$globalMod, right, workspace, r+1"
        "$globalMod, left, workspace, r-1"

        # move to the first empty workspace instantly with mainMod + [↓]
        "$globalMod, down, workspace, empty"

        # Special workspaces (scratchpad)
        "$globalMod, S, togglespecialworkspace,"
        "$globalMod $windowMod, S, movetoworkspacesilent, special"

        # Scroll through existing workspaces with mainMod + scroll
        "$globalMod, mouse_down, workspace, e+1"
        "$globalMod, mouse_up, workspace, e-1"

        # Work with windows

        # Window Actions
        "$windowMod, Q, killactive"
        "$windowMod, F4, killactive" # killactive, kill the window on focus
        "$windowMod, F, togglefloating," # toggle floating
        "$windowMod, G, togglegroup," # toggle tabbed
        "$windowMod SHIFT, return, fullscreen, 1" # toggle maximize
        "$windowMod, T, toggleopaque" # 

        # Cycle through windows with $windowMod+Tab
        "$windowMod, Tab, cyclenext"
        "$windowMod SHIFT, Tab, cyclenext, prev"
        "$windowMod, Return, layoutmsg, swapwithmaster"

        # Move focus inside group with $windowMod + Arrow Keys
        "$windowMod, left, changegroupactive, b"
        "$windowMod, right, changegroupactive, f"

        # Move active window to a workspace with mainMod + $windowMod + [0-9]
        "$globalMod $windowMod, 1, movetoworkspace, 1"
        "$globalMod $windowMod, 2, movetoworkspace, 2"
        "$globalMod $windowMod, 3, movetoworkspace, 3"
        "$globalMod $windowMod, 4, movetoworkspace, 4"
        "$globalMod $windowMod, 5, movetoworkspace, 5"
        "$globalMod $windowMod, 6, movetoworkspace, 6"
        "$globalMod $windowMod, 7, movetoworkspace, 7"
        "$globalMod $windowMod, 8, movetoworkspace, 8"
        "$globalMod $windowMod, 9, movetoworkspace, 9"
        "$globalMod $windowMod, 0, movetoworkspace, 10"

        # Move active window to a relative workspace with mainMod + CTRL + $windowMod + [←→]
        "$globalMod $windowMod, right, movetoworkspace, r+1"
        "$globalMod $windowMod, left, movetoworkspace, r-1"

        # Move active window around current workspace with mainMod + SHIFT + $windowMod [←→↑↓]
        "$globalMod SHIFT $windowMod, left, movewindow, l"
        "$globalMod SHIFT $windowMod, right, movewindow, r"
        "$globalMod SHIFT $windowMod, up, movewindow, u"
        "$globalMod SHIFT $windowMod, down, movewindow, d"
      ];

      # the e stands for repeatable, obviously
      binde = [
        # Volume control
        "$globalMod, F11, exec, $script volume-down" # decrease volume
        "$globalMod, F12, exec, $script volume-up" # increase volume
        ", XF86AudioLowerVolume, exec, $script volume-down" # decrease volume
        ", XF86AudioRaiseVolume, exec, $script volume-up" # increase volume

        # Brightness control
        ", XF86MonBrightnessUp, exec, $script brightness-up" # increase brightness
        ", XF86MonBrightnessDown, exec, $script brightness-down" # decrease brightness

        # Resize windows
        "$windowMod SHIFT, right, resizeactive, 10 0"
        "$windowMod SHIFT, left, resizeactive, -10 0"
        "$windowMod SHIFT, up, resizeactive, 0 -10"
        "$windowMod SHIFT, down, resizeactive, 0 10"
      ];

      # mouse "keybinds"
      bindm = [
        # Move/Resize windows with mainMod + LMB/RMB and dragging
        "$windowMod, mouse:272, movewindow"
        "$globalMod, mouse:272, movewindow"
        "$windowMod, mouse:273, resizewindow"
        "$globalMod, mouse:273, resizewindow"
      ];

      # the l stands for "switches", so "ledge" probably?
      bindl = [
        # Suspend if we close the laptop
        ", switch:on:Lid Switch, exec, $script suspend"
      ];
    };
  };
}
