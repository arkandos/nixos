{ config, lib, ... }:

let
  cfg = config.hyprland;
in
lib.mkIf cfg.enable {
	xdg.configFile."hypr/hyprlock.conf".text = ''
    background {
      monitor =
      path = ${cfg.wallpaper}
      blur_size = 6
      blur_passes = 3

      noise = 0.0117
      contrast = 0.8916
      brightness = 0.8172
      vibrancy = 0.1696
      vibrancy_darkness = 0.0
    }

    label {
      monitor =
      text = $TIME
      color = rgba(200, 200, 200, 1.0)
      font_size = 48
      font_family = Noto Sans

      position = 0, 80
      halign = center
      valign = center
    }
    input-field {
      monitor =
      size = 400, 50
      outline_thickness = 3
      dots_size = 0.33 # Scale of input-field height, 0.2 - 0.8
      dots_spacing = 0.15 # Scale of dots' absolute size, 0.0 - 1.0
      dots_center = false
      # dots_rounding = -1 # -1 default circle, -2 follow input-field rounding
      outer_color = rgb(b4637a)
      inner_color = rgb(200, 200, 200)
      font_color = rgb(10, 10, 10)
      fade_on_empty = true
      # fade_timeout = 1000 # Milliseconds before fade_on_empty is triggered.
      placeholder_text = <i>Password...</i> # Text rendered in the input box when it's empty.
      hide_input = false
      # rounding = -1 # -1 means complete rounding (circle/oval)

      position = 0, -20
      halign = center
      valign = center
    }

  '';
}
