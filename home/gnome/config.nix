{ config, lib, ... }:

with lib;

let
  cfg = config.gnome;
  gvariant = lib.hm.gvariant;
in
mkIf cfg.enable {
  home.file.".config/gnome-initial-setup-done".text = "yes";
  # home.file.".face".source = cfg.face;

  gtk.enable = true;
  gtk.gtk3.extraConfig = {
    gtk-application-prefer-dark-theme = 1;
  };
  gtk.gtk4.extraConfig = {
    gtk-application-prefer-dark-theme = 1;
  };

  dconf.enable = true;
  dconf.settings = with gvariant; {
    "org/gnome/mutter" = {
      attach-modal-dialogs = true;
      edge-tiling = true;
      experimental-features = ["scale-monitor-framebuffer"];
      workspaces-only-on-primary = false;
      dynamic-workspaces = true;
      focus-change-on-pointer-rest = true;
    };

    "org/gnome/settings-daemon/plugins/color" = {
      night-light-enabled = true;
    };

    "org/gnome/settings-daemon/plugins/power" = {
      power-button-action = "suspend";
      sleep-inactive-ac-timeout = 3600;
      sleep-inactive-ac-type = "suspend";
    };

    "org/gnome/settings-daemon/plugins/media-keys" = {
      mic-mute = ["<Primary>F10"]; # This is our mouse button
      play = ["Pause"];
    };

    "org/gnome/desktop/session" = {
      idle-delay = mkUint32 300;
    };

    "org/gnome/desktop/sound" = {
      allow-volume-above-100-percent = true;
      event-sounds = false;
    };

    "org/gnome/desktop/interface" = {
      clock-show-weekday = true;
      accent-color = "purple";
      color-scheme = "prefer-dark";
      enable-hot-corners = false;
      font-antialiasing = "rgba";
      cursor-theme = "Bibata-Modern-Classic";
      # gtk-theme = "Adwaita:dark";
      font-name = "Inter 11";
      document-font-name = "Inter 11";
      monospace-font-name = "Maple Mono NF 10";
      show-battery-percentage = true;
    };

    "org/gnome/desktop/background" = {
      picture-uri = cfg.wallpaper;
      picture-uri-dark = cfg.wallpaper;
    };

    "org/gnome/desktop/screensaver" = {
      picture-uri = cfg.wallpaper;
      lock-delay = mkUint32 30;
    };

    "org/gnome/desktop/input-sources" = {
      per-window = false;
      sources = [
        (mkTuple ["xkb" "us"])
        (mkTuple ["xkb" "de"])
      ];
      xkb-options = [
        "eurosign:e"
        "lv3:ralt_switch"
      ];
    };

    "org/gnome/desktop/peripherals/keyboard" = {
      numlock-state = cfg.numlockActive;
    };

    "org/gnome/desktop/peripherals/mouse" = {
      accel-profile = "flat";
      speed = cfg.mouseSpeed;
    };

    "org/gnome/desktop/peripherals/touchpad" = {
      tap-to-click = true;
      two-finger-scrolling-enabled = true;
    };

    "org/gnome/desktop/wm/preferences" = {
      focus-mode = "sloppy";
      resize-with-right-button = true;
    };

    "org/gnome/desktop/wm/keybindings" = {
      # Make <Alt>Tab switch windows by default
      switch-applications = ["<Primary><Alt>Tab"];
      switch-applications-backward = ["<Primary><Shift><Alt>Tab"];
      switch-panels = [];
      switch-panels-backward = [];
      switch-windows = ["<Alt>Tab"];
      switch-windows-backward = ["<Alt><Shift>Tab"];
    };

    "org/gnome/desktop/privacy" = {
      disable-camera = true;
    };

    "org/gnome/desktop/search-providers" = {
      disabled = [
        "org.gnome.Epiphany.desktop"
        "org.gnome.Characters.desktop"
        "org.gnome.seahorse.Application.desktop"
      ];

      enabled = [
        "org.gnome.Weather.desktop"
      ];

      sort-order = [
        "org.gnome.Calculator.desktop"
        "org.gnome.Weather.desktop"
        "org.gnome.Contacts.desktop"
        "org.gnome.Calendar.desktop"
        "org.gnome.Photos.desktop"
        "org.gnome.Nautilus.desktop"
        "org.gnome.Settings.desktop"
        "org.gnome.clocks.desktop"
        "org.gnome.Epiphany.desktop"
        "org.gnome.seahorse.Application.desktop"
        "org.gnome.Characters.desktop"
      ];
    };

    "org/gnome/shell" = {
      welcome-dialog-last-shown-version = "40.1";
      favorite-apps = cfg.favoriteApps;
    };

    "org/gnome/shell/app-switcher" = {
      current-workspace-only = true;
    };

    "org/gnome/shell/weather" = {
      automatic-location = true;
    };
    "org/gnome/GWeather4" = {
      temperature-unit = "centigrade";
    };

    "org/gnome/shell/extensions/vitals" = {
      hot-sensors = [
        "_memory_usage_"
        "_processor_usage_"
        "__network-rx_max__"
        "__network-tx_max__"
        "_storage_used_"
        "_battery_rate_"
      ];
      show-battery = true;
      show-fan = false;
      show-storage = true;
      show-voltage = false;
      storage-path = "/";
      battery-slot = 0;
    };

    "org/gnome/nautilus/preferences" = {
      click-policy = "single";
    };

    "org/gtk/settings/file-chooser" = {
      show-hidden = true;
      sort-directories-first = true;
    };

    "org/gtk/gtk4/settings/file-chooser" = {
      show-hidden = true;
      sort-directories-first = true;
    };

    "org/gnome/tweaks" = {
      show-extensions-notice = false;
    };
  };
}
