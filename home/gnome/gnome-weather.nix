{ config, lib, pkgs, ... }:

with builtins;
with lib;

let
  cfg = config.gnome;
in
mkIf cfg.enable {
  systemd.user.services.dconf-gnome-weather =
    let
      latRad = cfg.latitude * 0.017453292519943295; # 2pi / 360
      longRad = cfg.longitude * 0.017453292519943295;

      weatherLocation = "[<(uint32 2, <('Home', '', false, [(${toString latRad}, ${toString longRad})], [(${toString latRad}, ${toString longRad})])>)>]";

      script = pkgs.writeShellScriptBin "dconf-gnome-weather"
        ''
          # Weather uses tuples, which are not supported by dconf.settings
          ${pkgs.dconf}/bin/dconf write /org/gnome/Weather/locations "${weatherLocation}";

          ${pkgs.dconf}/bin/dconf write /org/gnome/shell/weather/locations "${weatherLocation}";
        '';
    in
    {
      Unit = {
        Description = "Write GWeather configuration, since it's incompatible with the dconf seting.";
        BindsTo = ["gnome-session.target"];
        After = ["dconf.service" "gnome-session.target"];
      };

      Install = {
        WantedBy = ["gnome-session.target"];
      };

      Service = {
        Type = "oneshot";
        ExecStart = "${script}/bin/dconf-gnome-weather";
      };
    };
}
