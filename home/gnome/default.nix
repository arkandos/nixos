{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.gnome;

  extensions = with pkgs.gnomeExtensions; [
    blur-my-shell
    vitals
    # pop-shell
    appindicator
  ];
in
{
  options.gnome = {
    enable = mkEnableOption "Configure Gnome and a bunch of other tools related to it";

    wallpaper = mkOption {
      type = types.str;
    };

    face = mkOption {
      type = types.str;
    };

    mouseSpeed = mkOption {
      type = types.float;
      default = 1.0;
    };

    numlockActive = mkOption {
      type = types.bool;
      default = true;
    };

    favoriteApps = mkOption {
      type = types.listOf types.str;
      default = [
        "firefox.desktop"
        "org.gnome.Geary.desktop"
        "org.gnome.Calendar.desktop"
        "org.gnome.Console.desktop"
        "org.gnome.Nautilus.desktop"
        "spotify.desktop"
      ];
    };

    longitude = mkOption {
      type = types.float;
    };

    latitude = mkOption {
      type = types.float;
    };
  };

  imports = [
    ./config.nix # gnome config
    ./gnome-weather.nix
  ];

  config = mkIf cfg.enable {  
    home.pointerCursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Classic";
      size = 24;
      gtk.enable = true;
      x11.enable = true;
    };

    home.file.".config/mimeapps.list".text = builtins.readFile ./mimeapps.list;

    dconf.settings."org/gnome/shell" = {
      disable-user-extensions = false;
      enabled-extensions = builtins.map (ext: ext.extensionUuid) extensions;
    };

    home.packages = extensions ++ (with pkgs; [
      gnome-tweaks
      zenity
      
      xdg-utils # xdg-open, etc
      wl-clipboard # clipboard for wayland
      pavucontrol # sound control
      firefox # browser
    ]);
  };
}
