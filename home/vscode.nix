{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.vscode;
in
{
  options.vscode = {
    enable = mkEnableOption "Enable VSCode with some settings and plugins installed.";

    extensions = mkOption {
      type = types.listOf types.package;
      default = [];
    };

    settings = mkOption {
      type = types.attrsOf types.anything;
      default = {};
    };
  };

  config = mkIf cfg.enable {
    programs.vscode = {
      enable = true;
      extensions =
        let
          fromNix = with pkgs.vscode-extensions; [
            eamodio.gitlens
          ];

          fromMarketplace = pkgs.vscode-utils.extensionsFromVscodeMarketplace [
            {
              name = "nord-wave";
              publisher = "dnlytras";
              version = "0.3.5";
              sha256 = "sha256-6zTU3vrfbcF6CI1Jhk4Ibx9LMfiNOWRXI9uu9hsRjzE=";
            }
          ];
        in
        fromNix ++ fromMarketplace ++ cfg.extensions;

      userSettings = {
        "window.newWindowDimensions" = "maximized";

        "editor.wordWrap" = "bounded";
        "editor.quickSuggestionsDelay"  = 50;
        "editor.renderLineHighlight" = "all";
        "editor.smoothScrolling" = true;
        "editor.wordWrapColumn" = 140;
        "editor.wrappingIndent" = "indent";
        "editor.fontLigatures" = true;
        "editor.detectIndentation" = true;
        "editor.rulers" = [80 120];
        "editor.bracketPairColorization.enabled" = false;
        "editor.fontFamily" = "'Maple Mono NF', 'Fira Code', monospace";

        "explorer.openEditors.visible" = 9;

        "breadcrumbs.enabled" = true;

        "workbench.startupEditor" = "newUntitledFile";
        "workbench.editor.showTabs" = false;
        "workbench.editor.openPositioning" = "first";
        "workbench.colorTheme" = "Nord Wave";
        "workbench.statusBar.visible" = true;

        "files.eol" = "\n";
        "files.insertFinalNewline" = true;
        "files.trimTrailingWhitespace" = true;

        "git.enableSmartCommit" = true;
        "git.openRepositoryInParentFolders" = "always";
        "gitlens.advanced.telemetry.enabled" = false;
        "gitlens.showWelcomeOnInstall" = false;
        "gitlens.showWhatsNewAfterUpgrades" = false;

        "window.menuBarVisibility" = "visible";
        "window.titleBarStyle" = "custom";
        "window.closeWhenEmpty" = true;
        "window.restoreWindows" = "none";

        "telemetry.enableTelemetry" = false;
        "telemetry.enableCrashReporter" = false;
        "telemetry.telemetryLevel" = "off";
        "security.workspace.trust.enabled" = false;

        "debug.onTaskErrors" = "abort";
      } // cfg.settings;
    };
  };
}
