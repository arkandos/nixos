{
  description = "yoshi's system configuration.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    impermanence.url = "github:nix-community/impermanence/master";

    # custom fork based on the-mikedavis/helix/driver reverting some incompatible changes
    helix.url = "github:joshi-monster/helix/driver";
    hosts.url = "github:StevenBlack/hosts";

    # git-rename-branch.url = "gitlab:arkandos/git-rename-branch";
    # git-gud.url = "gitlab:arkandos/git-gud";
    localhostd.url = "gitlab:arkandos/localhostd";
  };

  outputs = { self, nixpkgs, home-manager, impermanence, hosts, localhostd, ... }@inputs:
    let
      system = "x86_64-linux";

      lib = nixpkgs.lib;
      pkgs = import inputs.nixpkgs { config.allowUnfree = true; system = system; };

      insomnia = import ./programs/insomnia.nix { inherit pkgs; };
      teams = import ./programs/teams.nix { inherit pkgs lib; stdenv = pkgs.stdenv; };
      # desktopctl = import ./programs/desktopctl { inherit pkgs lib; };
      # git-rename-branch = inputs.git-rename-branch.defaultPackage.${system};
      helix = inputs.helix.packages.${system}.helix;

      nixconfig = {
        nixpkgs = {
          config = {
            allowUnfree = true;
            chromium.enableWideVine = true;
          };

          overlays = [
            (final: prev: {
              inherit insomnia teams helix;
            })
          ];
        };
      };

      common-modules = [
        {
          nix = {
            settings.trusted-users = [ "root" "arkan" ];
            settings.experimental-features = "nix-command flakes";
            gc = {
              automatic = true;
              persistent = true;
              randomizedDelaySec = "15m";
              dates = "13:15";
              options = "--delete-older-than 14d";
            };
          };

          # Disable the stub-ld - if we can run an app without the nixos dance, why not?
          environment.stub-ld.enable = false;

          # git is required for flakes
          environment.systemPackages = with pkgs; [ git git-crypt ];
        }
        home-manager.nixosModules.home-manager
        impermanence.nixosModules.impermanence
        hosts.nixosModule
        {
          networking.stevenBlackHosts = {
            enable = true;
            blockFakenews = true;
            blockGambling = true;
          };
        }
        localhostd.nixosModule
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
        }
        ./modules/core.nix
        ./modules/programs.nix
        ./modules/impermanence.nix
      ];
    in
    {
      nixosConfigurations = {
        valor = lib.nixosSystem {
          inherit system;
          modules = [ nixconfig ./hosts/valor ./users/arkan ] ++ common-modules;
          specialArgs = { inherit inputs; };
        };

        garen = lib.nixosSystem {
          inherit system;
          modules = [ nixconfig ./hosts/garen ./users/arkan ] ++ common-modules;
          specialArgs ={ inherit inputs; };
        };
      };
    };
}
