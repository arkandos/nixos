{ pkgs, lib, stdenv }:

let
  mkWebapp = import ./webapp.nix { inherit pkgs lib stdenv; };
in
mkWebapp {
  name = "Teams";
  desktopName = "Microsoft Teams";
  description = "Microsoft Teams";
  categories = ["Network" "InstantMessaging" ];
  url = https://teams.microsoft.com;
}
 
# creativity |]=3dS}BtmM7xyAa

