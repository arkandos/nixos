{ pkgs, ... }:

pkgs.discord.overrideAttrs (attrs: rec {
  version = "0.0.22";

  src = pkgs.fetchurl {
    url = "https://dl.discordapp.net/apps/linux/${version}/discord-${version}.tar.gz";
    sha256 = "sha256-F1xzdx4Em6Ref7HTe9EH7whx49iFc0DFpaQKdFquq6c=";
  };

  nativeBuildInputs = (attrs.nativeBuildInputs or []) ++ [ pkgs.makeWrapper ];

  postFixup = (attrs.postFixup or "") + ''
    wrapProgram $out/bin/discord --add-flags "--use-gl=desktop"
    wrapProgram $out/bin/Discord --add-flags "--use-gl=desktop"
  '';
})
