{ pkgs, lib, stdenv }:

with builtins;
with lib;

{
  name,
  desktopName ? name,
  description,
  url,
  width ? 1024,
  height ? 768,
  categories ? [ "Network" ]
} :
stdenv.mkDerivation {
  name = name;

  dontUnpack = true;

  nativeBuildInputs = with pkgs; [ makeWrapper copyDesktopItems ];

  installPhase = ''
    runHook preInstall

    makeWrapper '${pkgs.chromium}/bin/chromium' $out/bin/${name} \
      --add-flags '--user-data-dir=$(echo ~/.config/${name})' \
      --add-flags '--window-size=${toString width},${toString height}' \
      --add-flags '--app=${strings.escapeShellArg url}'

    runHook postInstall
  '';

  desktopItems = [
    (pkgs.makeDesktopItem {
      name = name;
      exec = name;
      desktopName = desktopName;
      comment = description;
      categories = categories;
    })
  ];

  meta = {
    description = description;
    homepage = url;
    downloadPage = url;
    platforms = pkgs.chromium.meta.platforms;
  };
}
