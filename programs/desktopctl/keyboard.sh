function toggle-xkb-layout {
    # We use kb_options to actually do the switch, so we only need to report the new layout

    # hyprctl devices -j | jq -r '.keyboards[].name' | while read keyName
    # do
    #     hyprctl switchxkblayout "$keyName" next
    # done
    # hyprctl switchxkblayout "$(hyprctl devices -j | jq -r '.keyboards[] | select (.main == true) | .name')" next

    # FIXME: the main keyboard is not the main keyboard, so we get the wrong value here.
    active_keymap="$(hyprctl devices -j | jq -r '.keyboards[] | select (.main == true) | .active_keymap')"
    notify-send -a desktopctl "Keyboard Layout" "$active_keymap"
}

