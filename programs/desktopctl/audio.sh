function _notify_volume {
    name=$(wpctl inspect "$1" | grep node.nick | cut -d '"' -f 2)
    volume=$(wpctl get-volume "$1" | cut -b 9-)
    if [[ "$volume" == *MUTED* ]]; then
        volume=0
    else
        volume=$(echo $volume | jaq '.*100|round')
    fi

    # TODO: Icons don't work because of nixos probably
    if [ $volume -eq 0 ]; then
        icon=audio-volume-muted
    elif [ $volume -lt 20 ]; then
        icon=audio-volume-low
    elif [ $volume -lt 50 ]; then
        icon=audio-volume-medium
    else
        icon=audio-volume-high
    fi

    if [ $volume -eq 0 ]; then
        volume="muted"
    else
        volume="$volume%"
    fi

    notify-send -e -a desktopctl -u low -i $icon -h string:x-dunst-stack-tag:volume "$name" "Volume: $volume"
}

# toggle mute on the main sink
function toggle-mute {
    wpctl set-mute @DEFAULT_SINK@ toggle
    _notify_volume @DEFAULT_SINK@
}

# toggle mute on the main source
function toggle-mic-mute {
    wpctl set-mute @DEFAULT_SOURCE@ toggle
    _notify_volume @DEFAULT_SOURCE@
}

# increase volume of the main output in steps
function volume-up {
    wpctl set-volume @DEFAULT_SINK@ 5%+
    _notify_volume @DEFAULT_SINK@
}

# decrease volume of the main output in steps
function volume-down {
    wpctl set-volume @DEFAULT_SINK@ 5%-
    _notify_volume @DEFAULT_SINK@
}

PLAYER_ORDER=%any,firefox,chromium
function _notify_playing {
    IFS=$'\n' read -rd $'\1' player status artist title < <(playerctl metadata -p "$PLAYER_ORDER" -f $'{{playerName}}\n{{status}}\n{{artist}}\n{{title}}\1')
    if [[ "$status" == "Playing" ]]; then
      notify-send -h string:x-dunst-stack-tag:playerctl -a "$player" "$artist" "$title" 
    fi
}

# play or pause the current player
function play-pause {
    playerctl -p "$PLAYER_ORDER" play-pause
    _notify_playing  
}

# skip to the next song
function play-next {
    playerctl -p "$PLAYER_ORDER" next
    _notify_playing
}

# skip to the beginning of the song, or the previous song
function play-prev {
    playerctl -p "$PLAYER_ORDER" previous
    _notify_playing
}
