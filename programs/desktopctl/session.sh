# logout exits the current session
function logout {
	hyprctl dispatch exit 0
}

# lock notifies the session that it should lock itself.
# you need to listen to this event yourself, e.g. using swayidle
function lock {
	loginctl lock-session
}

# suspends the machien
function suspend {
	systemctl suspend
}

# turns the computer off
function shutdown {
	systemctl poweroff
}

# reboot
function reboot {
	systemctl reboot
}

# goto workspace number, or up/down to move to next/prev
function goto-workspace {
	case "$1" in
		down|next)
		
			hyprctl dispatch workspace +1;;

		up|prev)
			hyprctl dispatch workspace -1;;
			
		*)
			hyprctl dispatch workspace $1;;
	esac
}

# listen to workspace changes, outputting json to be consumed by other tools
function listen-workspaces {
	declare focused_monitor_name
	# monitor name -> workspace id 
	declare -A active_workspaces=()
	# workspace id -> monitor name
	declare -A workspace_to_monitor_name=()
	# unfortunately, when we query for workspaces, we only get the name. so we need
	# to keep an additional map to relate them back to ids
	declare -A monitor_name_to_idx=()

	function _init {
		monitors=$(hyprctl -j monitors)
		focused_monitor_name=$(echo "$monitors" | jaq -r '.[] | select(.focused == true) | .name')

		active_workspaces=()
		while read -r name workspace; do
			active_workspaces["$name"]=$workspace
		done < <(echo "$monitors" | jaq -jr '.[] | .name, " ", .activeWorkspace.id, "\n"')
		
		monitor_name_to_id=()
		while read -r name id; do
			monitor_name_to_idx["$name"]=$(( $id + 1 ))
		done < <(echo "$monitors" | jaq -jr '.[] | .name, " ", .id, "\n"')
	
		workspace_to_monitor_name=()
		while read -r id monitor; do
			workspace_to_monitor_name["$id"]="$monitor"
		done < <(hyprctl -j workspaces | jaq -jr '.[] | .id, " ", .monitor, "\n"')
	}

	function _view {
		max_workspace_id=0
		for workspace in ${!workspace_to_monitor_name[@]}; do
			if [ $workspace -gt $max_workspace_id ]; then
				max_workspace_id=$workspace
			fi
		done	
	
		for workspace in $(seq 1 $max_workspace_id); do
				monitor_name="${workspace_to_monitor_name[$workspace]:-(unknown)}"
				monitor_idx=${monitor_name_to_idx["$monitor_name"]:-0}
			
				active=0
				if [ $workspace -eq ${active_workspaces["$monitor_name"]:-0} ]; then
					active=1
				fi
			
				focused=0
				if [[ $active -eq 1 && "$monitor_name" == "$focused_monitor_name" ]]; then
					focused=1
				fi
			
				echo "$workspace $active $focused $monitor_idx $monitor_name";
		# done
		done |
			jaq -c --raw-input -s \
				'{
					workspaces: [
						split("\n")
						| .[]
						| select(. != "")
						| split(" ")
						| {
								workspace: .[0] | tonumber,
								active: .[1] == "1",
								focused: .[2] == "1",
								monitor: .[3] | tonumber,
								monitor_name: .[4:] | join(" ")
							}
					] | sort_by(.workspace)
				}'
	}

	_init
	_view

	socat -U - UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | while read -r line; do
		case ${line%>>*} in
			workspace) # >>workspace id
				tmp=${line#*>>}
				active_workspaces["$focused_monitor_name"]=$tmp
				workspace_to_monitor_name[$tmp]="$focused_monitor_name"
				;;

			focusedmon) # >>monitor name,workspace id
				tmp=${line#*>>}
				focused_monitor_name=${tmp%,*}
				active_workspaces["$focused_monitor_name"]=${tmp#*,}
				;;

			createworkspace) # >>workspace id
			  tmp=${line#*>>}
				workspace_to_monitor_name[$tmp]="$focused_monitor_name"
				;;

			destroyworkspace) #>>workspace id
				tmp=${line#*>>}
				unset workspace_to_monitor_name[$tmp]
				;;

			monitoradded|monitorremoved)
				# this rarely happens - don't be smart
				_init
				;;

			*)
				continue # skip all other events
				;;
		esac

		_view
	done
}
