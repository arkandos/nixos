function _notify_brightness_result {
	percentage=$(cut -d , -f 4 <<< $1)
  notify-send -e -u low -a desktopctl -h string:x-dunst-stack-tag:brightness "Screen" "Brightness $percentage"
}

function brightness-up {
	out=$(brightnessctl -m --class=backlight --exponent=1.5 set +5%)
	_notify_brightness_result "$out"
}

function brightness-down {
	out=$(brightnessctl -m --class=backlight --exponent=1.5 set 5%-)
	_notify_brightness_result "$out"
}


# turns all screens off, and locks the screen
function screen-off {
	lock
	hyprctl dispatch dpms off
}

# turn screen back on
function screen-on {
  hyprctl dispatch dpms on
}

function _screenshot_impl {
    if [ -n "${XDG_PICTURES_DIR:-}" ]; then
        picture_dir="$XDG_PICTURES_DIR"
    else
        picture_dir="$HOME/Pictures"
    fi

    save_file="$(date +'Screenshot from %Y-%m-%d %H-%M-%S.png')"
    save_dir="$picture_dir/Screenshots"
    tmp_file="/tmp/screenshot.png"

    swpy_dir="$HOME/.config/swappy"
    mkdir -p $save_dir
    mkdir -p $swpy_dir

    echo -e "[Default]\nsave_dir=$save_dir\nsave_filename_format=$save_file" > $swpy_dir/config

    grimblast --freeze copysave $1 "$tmp_file" && swappy -f "$tmp_file"

    rm "$tmp_file"

    if [ -f "$save_dir/$save_file" ]; then
        notify-send -a desktopctl "Screen" "$save_file"
    fi
}

function screenshot-screen {
    _screenshot_impl screen
}

function screenshot-monitor {
    _screenshot_impl output
}

function screenshot-area {
    _screenshot_impl area
}
