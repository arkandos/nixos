{ pkgs, lib }:

let
  name = "desktopctl";

  runtimeDependencies = with pkgs; [
    # session, notifications, core
    systemd hyprland libnotify socat jaq
    # audio, music
    wireplumber playerctl
    # screen brightness
    brightnessctl
    # screenshots
    grimblast swappy wl-clipboard
  ];

  prelude = ''
    #!${pkgs.runtimeShell}
    set -o errexit
    set -o nounset
    set -o pipefail
    export PATH="${lib.makeBinPath runtimeDependencies}:$PATH"
  '';

  buildScript = ''
    file="$out$destination"
    mkdir -p "$(dirname "$file")"
    cat "$preludePath" "$files"/* > "$file"
    chmod +x "$file"
    eval "$checkPhase"
  '';
in pkgs.runCommandLocal name {
  meta = { mainProgram = name; };

  files = lib.sources.sourceFilesBySuffices ./. [ ".sh" ];

  prelude = prelude;
  passAsFile = [ "prelude" ];

  destination = "/bin/${name}";
  executable = true;
} buildScript
