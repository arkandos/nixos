{ pkgs, ... }:

pkgs.insomnia.overrideAttrs (attrs: rec {
    nativeBuildInputs = (attrs.nativeBuildInputs or []) ++ [ pkgs.makeWrapper ];

    postFixup = (attrs.postFixup or "") + ''
        wrapProgram $out/bin/insomnia --add-flags "--no-sandbox"
    '';
})
