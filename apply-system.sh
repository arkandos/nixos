#!/bin/sh
pushd $(dirname $(realpath $0))
sudo nixos-rebuild switch --flake '.#' --keep-going $*
popd
