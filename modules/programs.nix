{ pkgs, ... }:

{
  # is it relevant to fix the broken system -> system package
  # is it part of a nice desktop environment -> home package
  # is it something I want -> user package
  environment.systemPackages = with pkgs; [
    # Misc
    neofetch # the most important package on every system
    mkpasswd # need to to set initialHashedPassword

    # Files
    helix nano vim # Editors
    bat # Bat is a better cat
    eza # Exa is a better ls
    duf # duf is a better df
    du-dust # dust is a better du
    zip unzip unrar
    file

    fzf # Fuzzy Search files by name
    ranger # CLI finder

    # System Monitoring
    inxi # Get information about the current system
    htop iotop iftop
    lsof inetutils pciutils usbutils pstree

    # Nix tools
    # nix-prefetch-scripts nix-update nixpkgs-review nix-index

    # Networking
    wget curl # downloaders
    w3m # console browser
    openssl gnupg
  ];

  environment.shellAliases = {
    # interactive file ops
    cp = "cp -i";
    mv = "mv -i";
    rm = "rm -iv";
    rmdir = "rm -fr";
    mkdir = "mkdir -p";

    ps = "ps auxf"; # print everything
    ping = "ping -c 5"; # 5 pings is enough
    less = "less -R"; # support ANSI control sequences

    # better cd
    cd = "z";
    home = "cd ~";
    src = "cd ~/Projects/private";
    cry = "cd ~/Projects/creativITy";
    "cd.." = "cd ..";
    "." = "cd $(git rev-parse --show-toplevel || pwd)";
    ".." = "cd ..";
    "..." = "cd ../..";
    "...." = "cd ../../..";
    "....." = "cd ../../../..";
    bd = "cd $OLDPWD";

    # better ls
    ls = "eza -a -F -h --color=always";
    la = "ls -Alh";
    lx = "ls -lXBh"; # sort by ext
    lk = "ls -lSrh"; # sort by size
    # lc = "ls -lcrh"; # sort by change # c does not exist on exa
    lu = "ls -lurh"; # sort by access
    lr = "ls -lRh"; # recursive
    ll = "ls -alh | less"; # ls | less

    # tar is stupid
    mktar = "tar -cvf";
    mkbz2 = "tar -cvjf";
    mkgz = "tar -cvzf";
    untar = "tar -xvf";
    unbz2 = "tar -xvjf";
    ungz = "tar -xvzf";

    g = "git";
    gc = "git commit";
    gs = "git status";

    cat = "bat";
    du = "dust";
    df = "duf";

    x = "exit";
  };

  environment.variables.EDITOR = "hx";
}
