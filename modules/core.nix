{ lib, pkgs, ... }:

with builtins;
with lib;

{
  boot = {
    loader = {
      systemd-boot = {
        enable = true;
        memtest86.enable = true;
        # this is a security problem, probably
        editor = false;
        consoleMode = "max";
      };

      efi.canTouchEfiVariables = true;

      timeout = 1;
    };

    # plymouth = {
    #   enable = true;
    #   theme = "catppuccin-macchiato";
    #   themePackages = [ pkgs.catppuccin-plymouth ];
    # };

    kernelParams = [
      "quiet"
      "loglevel=3"
      "systemd.show_status=auto"
      "udev.log_level=3"
      "rd.udev.log_level=3"
      "vt.global_cursor_default=0"
    ];
  };

  # Don't replace the running kernel, what are you doing?
  security.protectKernelImage = true;

  console = {
    # earlySetup = true;
    # font = "${pkgs.terminus_font}/share/consolefonts/ter-v32n.psf.gz";
    # packages = [ pkgs.terminus_font ];
    # keyMap = "us";
    useXkbConfig = true;
  };

  # CPU
  hardware.enableRedistributableFirmware = true;
  hardware.cpu.intel.updateMicrocode = true;
  hardware.cpu.amd.updateMicrocode = true;

  # Power Management
  powerManagement.cpuFreqGovernor = "powersave";
  services.power-profiles-daemon.enable = false;
  services.tlp.enable = true;

  # Low swappiness to keep things in memory and improve SSD lifetime
  boot.kernel.sysctl."vm.swappiness" = 1;

  # Virtualisation
  virtualisation.podman = {
    enable = true;
    dockerSocket.enable = true;
    dockerCompat = true;
    extraPackages = with pkgs; [ slirp4netns fuse-overlayfs ];
  };

  # virtualisation.waydroid.enable = true;

  # Userland niceness
  security.rtkit.enable = true;

  # Localization
  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.supportedLocales = [ "en_US.UTF-8/UTF-8" "de_DE.UTF-8/UTF-8" ];

  # Networking
  networking.networkmanager.enable = true;
  networking.networkmanager.wifi.powersave = true;
  # don't wait for network on startup
  systemd.services.NetworkManager-wait-online.enable = false;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # netwokring.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  networking.extraHosts = ''
    190.115.31.218 sci-hub.ru
  '';

  # services.dnsmasq = {
  #   enable = true;
  #   settings = {
  #     server = [
  #       "8.8.8.8"
  #       "4.4.4.4"
  #     ];
  #     address = [
  #       "/.local/127.0.0.1"
  #       "/.${config.networking.hostName}/127.0.0.1"
  #     ];
  #   };
  # };

  services.localhostd = {
    enable = true;
    https = true;
    caCert = "/etc/nixos/secrets/localhostdCA.pem";
    caKey = "/etc/nixos/secrets/localhostdCA-key.pem";
  };

  security.pki.certificateFiles = [
    (builtins.fetchurl {
      url = "file:///etc/nixos/secrets/localhostdCA.pem";
      sha256 = "sha256:0b94nwj1sypw0wmn5m0pyc46cihsj227fj0wldizk986xc1vsapd";
    })
  ];

  # Sound
  services.pulseaudio.enable = false;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
    alsa.support32Bit = true;
  };


  # Some apps require special binaries in /bin.
  # So we symlink them here; this is easier than patching all of them.
  systemd.services.bin-apps-fix = {
    enable = true;
    wantedBy = ["graphical.target"];
    script = ''
      mkdir -m 0755 -p /lib64
      [ ! -e /lib64/ld-linux-x86-64.so.2 ] && ln -s ${pkgs.glibc.out}/lib64/ld-linux-x86-64.so.2 /lib64/ld-linux-x86-64.so.2
      [ ! -e /bin/bash ] && ln -s /run/current-system/sw/bin/bash /bin/bash;
      [ ! -e /bin/ps ] && ln -s /run/current-system/sw/bin/ps /bin/ps;
      [ ! -e /usr/bin/pkexec ] && ln -s /run/wrappers/bin/pkexec /usr/bin/pkexec;
      exit 0;
    '';
  };

  # Make sudo print out stars while typing the password
  # Since we are in a "fresh" system every time we reboot, disable the sudo lecture alltogether.
  security.sudo.extraConfig = ''
    Defaults pwfeedback
    Defaults lecture = never
  '';

  # Security tweaks borrowed from fufexan, who got them from hlissner
  # fufexan: https://github.com/fufexan/dotfiles/blob/main/modules/security.nix
  # hlissner: https://github.com/hlissner/dotfiles/blob/master/modules/security.nix

  boot.kernel.sysctl = {
    # The Magic SysRq key is a key combo that allows users connected to the
    # system console of a Linux kernel to perform some low-level commands.
    # Disable it, since we don't need it, and is a potential security concern.
    "kernel.sysrq" = 0;

    ## TCP hardening
    # Prevent bogus ICMP errors from filling up logs.
    "net.ipv4.icmp_ignore_bogus_error_responses" = 1;
    # Reverse path filtering causes the kernel to do source validation of
    # packets received from all interfaces. This can mitigate IP spoofing.
    "net.ipv4.conf.default.rp_filter" = 1;
    "net.ipv4.conf.all.rp_filter" = 1;
    # Do not accept IP source route packets (we're not a router)
    "net.ipv4.conf.all.accept_source_route" = 0;
    "net.ipv6.conf.all.accept_source_route" = 0;
    # Don't send ICMP redirects (again, we're on a router)
    "net.ipv4.conf.all.send_redirects" = 0;
    "net.ipv4.conf.default.send_redirects" = 0;
    # Refuse ICMP redirects (MITM mitigations)
    "net.ipv4.conf.all.accept_redirects" = 0;
    "net.ipv4.conf.default.accept_redirects" = 0;
    "net.ipv4.conf.all.secure_redirects" = 0;
    "net.ipv4.conf.default.secure_redirects" = 0;
    "net.ipv6.conf.all.accept_redirects" = 0;
    "net.ipv6.conf.default.accept_redirects" = 0;
    # Protects against SYN flood attacks
    "net.ipv4.tcp_syncookies" = 1;
    # Incomplete protection again TIME-WAIT assassination
    "net.ipv4.tcp_rfc1337" = 1;

    ## TCP optimization
    # TCP Fast Open is a TCP extension that reduces network latency by packing
    # data in the sender’s initial TCP SYN. Setting 3 = enable TCP Fast Open for
    # both incoming and outgoing connections:
    "net.ipv4.tcp_fastopen" = 3;
    # Bufferbloat mitigations + slight improvement in throughput & latency
    "net.ipv4.tcp_congestion_control" = "bbr";
    "net.core.default_qdisc" = "cake";
  };

  boot.kernelModules = ["tcp_bbr"];

  # From the-mikedavis/dotfiles
  security.pam.loginLimits = [
    { domain = "*"; type = "soft"; item = "nofile"; value = "65535"; }
  ];
}
