{ config, lib, ... }:

with builtins;
with lib;

let
    cfg = config.impermanence;
in
{
  options = {
    impermanence = {
      enable = mkEnableOption "File System Settings, Impermanence";

      rootSize = mkOption {
        type = types.strMatching "[[:digit:]]+(K|M|G|T)";
        default = "2G";
      };

      boot.device = mkOption {
        type = types.str;
        default = "/dev/disk/by-label/boot";
      };

      boot.fsType = mkOption {
        type = types.str;
        default = "vfat";
      };

      store.device = mkOption {
        type = types.str;
        default = "/dev/disk/by-label/nixos";
      };

      store.fsType = mkOption {
        type = types.str;
        default = "ext4";
      };

      home.device = mkOption {
        type = types.str;
        default = "/dev/disk/by-label/persist";
      };

      home.fsType = mkOption {
        type = types.str;
        default = "ext4";
      };
    };
  };

  config = mkIf cfg.enable {
    fileSystems = {
      "/" = {
        device = "none";
        fsType = "tmpfs";
        options = ["defaults" "size=${cfg.rootSize}" "mode=755"];
      };

      "/nix" = cfg.store;
      "/boot" = cfg.boot;

      # neededForBoot affects the order of mounts, such that we can be sure
      # it is mounted after the nix store exists
      "/nix/persist/users" = cfg.home // { neededForBoot = true; };
    };

    environment.persistence."/nix/persist/system" = {
      hideMounts = true;
      directories = [
        "/etc/nixos" # nixos configuration
        "/etc/NetworkManager" # saved wifi's and stuff
        "/var/log" # log files should persist so you can debug after a crash
        "/var/lib/systemd/coredump" # see log files
        "/var/lib/systemd/timers" # save persisted timer timestamps such that they work
        "/var/lib/container" # Docker
        # "/var/lib/waydroid" # Waydroid

        "/root/.cache/nix" # nix fetcher cache

        # additional stuff from the wiki
        "/var/lib/bluetooth"
        "/var/lib/nixos"
        { directory = "/var/lib/colord"; user = "colord"; group = "colord"; mode = "u=rwx,g=rx,o="; }
      ];

      # This produces a conflict now -
      # TODO: check if it is still needed.
      # files = [
      #   "/etc/machine-id" # makes logs easier to read.
      # ];
    };

    environment.persistence."/nix/persist/users".hideMounts = true;

    # Cleanup SSD
    services.fstrim.enable = true;

    # Cannot have mutable users if impermanence is enabled, this does not make sense!
    users.mutableUsers = false;
  };
}
