{  pkgs, ... }:

with builtins;

{
  # Hardware accelleration
  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [ libva vaapiVdpau libvdpau-va-gl ];
    extraPackages32 = with pkgs.pkgsi686Linux; [ vaapiVdpau libvdpau-va-gl ];
  };

  # Configure keymap in X11
  services.xserver.xkb.layout = "us,de";
  # no gnome needed to toggle languages
  # TODO: this has some trouble to detect the main keyboard - it works,
  # but the keyboard is not detected as the "main" one, so we report the wrong keymap afterwards?
  # Maybe we should do it in bash after all instead of xkb magic
  services.xserver.xkb.options = "grp:win_space_toggle,eurosign:e,lv3:ralt_switch";
  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Fonts
  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-cjk-sans
    noto-fonts-emoji

    font-awesome
    maple-mono-NF
    # (nerdfonts.override { fonts = ["FiraCode"]; })

    libre-baskerville
    inter
  ];

  fonts.fontconfig = {
    defaultFonts = {
      serif = [ "Libre Baskerville" "Noto Color Emoji" ];
      sansSerif = [ "Inter" "Noto Color Emoji" ];
      monospace = [ "Maple Mono NF" "Noto Color Emoji" ];
      emoji = [ "Noto Color Emoji" ];
    };
  };

  # Desktop Environment
  services.xserver.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  services.xserver.displayManager.gdm.enable = true;

  programs.dconf.profiles.gdm.databases = [{
    settings = {
      "org/gnome/mutter" = {
        experimental-features = ["scale-monitor-framebuffer"];
      }; 
    };
  }];
  
  programs.dconf.enable = true;
  services.udev.packages = [ pkgs.gnome-settings-daemon ];
  
  # Environment setup for Wayland
  # environment.variables.NIXOS_OZONE_WL = "1";
  environment.sessionVariables = {
    NIXOS_OZONE_WL = "1";
    GDK_BACKEND = "wayland";
    QT_QPA_PLATFORM = "wayland";
    QT_AUTO_SCREEN_SCALE_FACTOR = "1";
    SDL_VIDEODRIVER = "wayland";
    XDG_SESSION_TYPE = "wayland";
    MOZ_ENABLE_WAYLAND = "1";
    EGL_PLATFORM = "wayland";
  };
}
