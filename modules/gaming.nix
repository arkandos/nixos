{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    discord
    (lutris.override {
      extraPkgs = pkgs: with pkgs; [
        gamescope
        xorg.libXcursor
        xorg.libXi
        xorg.libXinerama
        xorg.libXScrnSaver
        libpng
        libpulseaudio
        libvorbis
        stdenv.cc.cc.lib
        libkrb5
        keyutils
      ];
    })
    # prismlauncher
  ];

  programs.steam.enable = true;

  programs.gamemode = {
    enable = true;
    enableRenice = true;

    settings = {
      general = { renice = 10; };
      custom = {
        start = "${pkgs.libnotify}/bin/notify-send 'GameMode enabled'";
        end = "${pkgs.libnotify}/bin/notify-send 'GameMode disabled'";
      };
    };
  };
}
