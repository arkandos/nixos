#!/bin/sh
pushd $(dirname $(realpath $0))
if [ -z "$1" ]; then
    nix flake update
else
    nix flake lock --update-input "$1"
fi
popd
